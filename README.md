# CONTENTS

 * Introduction
 * Requirements
 * Installation
 * How Ara components work?


# INTRODUCTION:

Ara (أرى) is a drupal theme that embraces **modern** front-end technologies.
Ex: [bourbon/neat](http://bourbon.io/) and
[material design](http://www.google.com/design/spec/material-design/).

It makes "[**component** based design](http://patternlab.io/about.html)"
 (atomic design) concepts a reality in drupal front-end.

Ara makes drupal theming a breeze. You don't need to write heavy css/js files,
as Ara does not try to re-invent the wheel. Instead it brings to you popular
modern front-end **frameworks** like [Lumx](http://ui.lumapps.com/) and
[Polymer](https://www.polymer-project.org/).

These frameworks are not loaded as a whole. Displayed components call for their
needed framework parts. This ensures your pages load faster as they are **not
bloated** with unnecessary css/js code.

While Ara facilitates using external frameworks, it does not force you to use
them. It is so **flexible** that anybody can use it as a base theme, and adopt
own theming pattern.

Ara supports **RTL** and **BiDi**. RTL assets are only compiled/served if
requested in settings.

Technologies used in Ara include:
 Sass, Lib-sass, Typescript, Gulp, BrowserSync, Bourbon/Neat, Lumx, Angular,
 Polymer...

 * Project page:
   https://www.drupal.org/sandbox/ahwebd/2668242

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2668242


# REQUIREMENTS

**Note:** You will find below example commands for Linux **Ubuntu** that you
can adapt to your OS.


* ## Package managers **"npm"** and **"RubyGems"**:

```bash
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install nodejs ruby
# Update npm:
sudo npm install npm -g
```

Detailed instructions:
* https://nodejs.org/en/download/package-manager/
* https://docs.npmjs.com/getting-started/installing-node
* https://www.ruby-lang.org/en/documentation/installation/#apt


* ## **Gulp**, **Bower**, **Bourbon** and **Neat**:

```bash
sudo npm install --global gulp-cli bower
sudo gem install bourbon neat
```

* https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md
* http://bower.io/#install-bower
* https://github.com/thoughtbot/bourbon#installation
* https://github.com/thoughtbot/neat#installation


# INSTALLATION

* Before you begin, make sure all the [**requirements**]
(https://ahwebd.gitbooks.io/ara-theme-documentation/content/requirements.html)
are fulfilled.
* You will find example commands for Linux **Ubuntu** that you can adapt to
your OS.

# **1. Ara theme**:

As Ara is still a sandbox project, there is no releases available for download
on drupal.org. An alternate method to get it is cloning with git:
```bash
cd YOUR/DRUPAL/PATH/themes
git clone --branch 8.x-1.x https://git.drupal.org/sandbox/ahwebd/2668242.git ara
```

* #### **npm packages**:

Install the needed npm packages locally inside ara directory:
```bash
cd ara
npm install
```
This will create a new directory "node_modules" inside ara.

* #### **Configuration for gulpfile.js**:

Make a copy of "ara/example.gulpfile-config.json" inside ara and rename it to
"gulpfile-config.json".
Open it and configure **browser-sync** to point to your development site:

```javascript
    "browserSync" : {
      "proxy" : "mysite.dev"
    },
```
Change **proxy** value to your dev site url.

* #### **Libraries and frameworks**:

Inside ara directory:
```bash
# Create lib directory:
mkdir lib
cd lib
# Install bourbon and neat:
bourbon install
neat install
# Install LumX:
mkdir lumx
cd lumx
bower install lumx
cd ..
```

* #### **Compile assets**:

Run gulp inside ara directory:
```bash
gulp
```
To stop gulp press "Ctrl+Shift+c".


# **2. Subthemes**:

Ara is designed as a base theme to hold functions and logic related code,
and you need to create subtheme(s) that will hold site specific settings and
theming.

If you have multiple sites and in order to avoid duplicating common settings it
is recommended to create a subtheme that will hold these common settings and
serve as a base theme for site specific subthemes.

To facilitate creating Ara subthemes, example subthemes are provided: Bouquet
and Tulip.

Bouquet is a subtheme of Ara, and Tulip is a subtheme of Bouquet.

As these subthemes are still sandbox projects, there is no releases available
for download on drupal.org. An alternate method to get them is cloning with git:
```bash
cd YOUR/DRUPAL/PATH/themes
git clone -b 8.x-1.x https://git.drupal.org/sandbox/ahwebd/2668260.git bouquet
git clone -b 8.x-1.x https://git.drupal.org/sandbox/ahwebd/2668262.git tulip
```
These subthemes serve as examples and guides, so don't use them directly.
Instead make copies with different names and edit with your own settings.
This will avoid losing your custom settings when you update the provided
example subthemes.

Do the following:

* #### **Make copies of bouquet and tulip and rename files**:

```bash
cd MY/DRUPAL/PATH/themes
cp -r bouquet mybouquet
cd mybouquet
mv bouquet.info.yml mybouquet.info.yml
mv bouquet.libraries.yml mybouquet.libraries.yml
mv bouquet.theme mybouquet.theme
```
Open mybouquet.info.yml and change all instances of "bouquet" to "mybouquet"

Make a copy of "ara/example.gulpfile-config.json" inside mybouquet and rename
it to "gulpfile-config.json".
Open it and configure **browser-sync** to point to your development site:

```javascript
    "browserSync" : {
      "proxy" : "mysite.dev"
    },
```

Run gulp inside mybouquet directory:
```bash
gulp
```

Repeat the above steps with tulip.

Now make "mytulip" your active theme.


# How Ara components work?

There are 2 types of Ara components:
 * Components with twig files which load assets when enabled.
 * Components without twig files which are meant to be called by other
 components.

Lets discuss the first type.

Every part and bit in a drupal page (regions, blocks, buttons, textfields ...)
is generated with a twig file.

Before we begin you need to be familiar with the general concept of twig file
name suggestions, see:

https://www.drupal.org/node/2186401

https://www.drupal.org/node/1906392

https://www.drupal.org/node/2358785


Ara theme will add for each enabled component a new twig file name suggestion
pointing to the component twig file and overriding an existing suggestion. When
active, the component twig file will load the component library (css/js)
responsible for the new styling of the page element.

Lets look at this in more detail using an example:

Assume we want to style all the submit buttons in the page using Lumx framework.

Looking at a submit button html in the page source (with twig debug enabled) we
find:
```html
<!-- FILE NAME SUGGESTIONS:
   * input--submit.html.twig
   x input.html.twig
-->
```
From the above we understand that we want to override "input--submit.html.twig".

We look for an Ara component having a twig file with a name starting with
"input--submit". We find "input--submit--lumx-ara.html.twig" in
"ara/components/atoms/lumx/inputs/submit".

Note: If we don't find a corresponding component, or the available component
does not suit our page styling, we can easily create a new one.

To enable this component we add the following entry in
"YOURSUBTHEME/settings/components/enabled-components.yml"
```yml
input:
  submit:
    framework: lumx
```
then we clear the cache (drush command: drush cr).

Ara will now add a new twig file name suggestion:
"input--submit--lumx-ara.html.twig" with a priority that is exactly above
"input--submit.html.twig".

"input--submit--lumx-ara.html.twig" will load the library defining the needed
css/js files responsible for styling the button. This file will also add a
class name to the button element "input--submit--lumx-ara".

The next step is to customize the submit buttons using "settings.js.yml"
residing in "YOURSUBTHEME/settings/components/atoms/lumx/inputs/submit/".

Refer to http://ui.lumapps.com/components/button for the available settings.

Any setting value preceded by "gs." will be pulled from the global settings you
define in "YOURSUBTHEME/settings/settings.js.yml". This will make it easier to
have common settings between different components, and have colors and sizes
consistent in your page.

Notes:
If you have multiple Ara subthemes chained:

* You can enable components in any subtheme.
* Component settings defined in different subthemes will be merged while
ensuring  priority is given according to subthemes order.
