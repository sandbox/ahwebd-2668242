(function (domready, ara, drupalSettings) {
  "use strict";

  // Component name.
  var cn = 'block--searchform--header--lumx';
  // Component class name.
  var cl = cn + '-ara';
  // Component selector.
  var sel = '.' + cl;

  // Add angular module.
  ara.ngModsAdd('lumx.search-filter');

  // Get component settings.
  var s = JSON.parse(drupalSettings.ara[cn]) || {};
  s = s.searchFilter || {};

  // Prepare search filter attributes.
  var sfAttrs =
    (ara.isSet(() => s.closed)? s.closed : "") +
    (ara.isSet(() => s.theme)? ' theme="' + s.theme + '"' : "") +
    (ara.isSet(() => s.placeholder)? ' placeholder="' + ara.t(s.placeholder) + '"' : "") +
    (ara.isSet(() => s.filterWidth)? ' filter-width="' + s.filterWidth + '"' : "") +
    (ara.isSet(() => s.position)? ' position="' + s.position + '"' : "") +
    (ara.isSet(() => s.model)? ' model="' + s.model + '"' : "");

  domready(function () {
    var cw = document.createElement("div");
    cw.innerHTML = ara.qs(sel).outerHTML;
    var c = <HTMLElement>cw.firstChild;

    c.innerHTML +=
      '<form action="/' + drupalSettings.path.pathPrefix + 'search/node" method="get" accept-charset="UTF-8">\n' +
      '  <lx-search-filter ' + sfAttrs +'></lx-search-filter>' +
      '</form>';

    ara.loadedLastFunctions.push(function () {
      ara.qs(sel + ' input[type=text]').setAttribute("name", "keys");
    });

    // Copy the styled component back to the DOM.
    ara.qs(sel).outerHTML = cw.innerHTML;
  });
})(window['domready'], window['ara'], window['drupalSettings']);
