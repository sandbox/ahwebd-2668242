(function (domready, ara, drupalSettings, angular) {
  "use strict";

  // Component name.
  var cn = 'block--userlogin--header--lumx';
  // Component class name.
  var cl = cn + '-ara';
  // Component selector.
  var sel = '.' + cl;

  // Define angular module and controller for "dialog"
  angular.module(cl, ['lumx.utils.utils', 'lumx.dialog']).
  controller(cl + '-dialogController', function($scope, LxDialogService) {
    $scope.opendDialog = function(dialogId) {
      LxDialogService.open(dialogId);
    };
  });
  // Add the module.
  ara.ngMods.push(cl);

  // Get component settings.
  var s = JSON.parse(drupalSettings.ara[cn]) || {};

  domready(function () {
    // Put component inside a parent wrapper to allow assigning outerHTML.
    // Using a wrapper also allows manipulation outside the DOM,
    // which is more efficient.
    var cw = document.createElement("div");
    cw.innerHTML = ara.qs(sel).outerHTML;
    var c = <HTMLElement>cw.firstChild;

    c.outerHTML = '<div ng-controller="' + cl + '-dialogController"' +
      'class="' + cl + '-container">' +
      '<button ng-click="opendDialog(\'' + cl + '-dialog\')">' +
      ara.t('Login') +
      '</button>' +
      '<lx-dialog class="dialog" id="' + cl + '-dialog">' +
      c.outerHTML +
      '</lx-dialog>' +
      '</div>';

    // Style buttons.
    ara.make.lumxButton( ara.qs(sel + '-container > button', cw), s.dialogBtn );
    ara.make.lumxButton( ara.qs(sel + ' [type=submit]', cw), s.submitBtn );
    ara.make.lumxButton( ara.qs(sel + ' a.create-account-link', cw), s.createAccountLink );
    ara.make.lumxButton( ara.qs(sel + ' a.request-password-link', cw), s.requestPasswordLink );

    // Copy the styled component back to the DOM.
    ara.qs(sel).outerHTML = cw.innerHTML;
  });
})(window['domready'], window['ara'], window['drupalSettings'], window['angular']);
