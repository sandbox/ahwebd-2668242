(function (ara) {
  "use strict";

  /**
   * Function to make a polymer button from another element.
   *
   * @param {HTMLElement} element
   * @param {object} settings
   *
   * @return {HTMLElement}
   *   The styled polymer button.
   */
  ara.make.polymerButton = function (element, settings) {

    // Check if provided element exists.
    if (!element) { return null; }

    // Preprocess element.
    var retObj = ara.preprocessElement(element, 'polymer', settings);
    if (retObj) {
      var el = retObj.originalElement;
      settings = retObj.settings;
    } else {
      return null;
    }

    var paperBtn;
    if (el.tagName.toLowerCase() === 'input') {
      el = ara.replaceTagName(el, 'button');
      paperBtn = document.createElement('paper-button');
      if (el.hasAttribute('value')) {
        paperBtn.innerHTML += el.getAttribute('value');
      }
      el.appendChild(paperBtn);
      el.className += ' no-appearance';
    } else {
      el = ara.replaceTagName(el, 'paper-button');
      paperBtn = el;
    }

    for (var setting in settings) {
      if (settings[setting] !== false && setting !== 'weight') {
        if (setting !== 'icon') {
          paperBtn.setAttribute(setting, settings[setting]);
        } else {
          paperBtn.innerHTML = '<iron-icon icon="'
          + settings.icon['icon-set'] + ':' + settings.icon['icon-name']
          + '"></iron-icon>' + paperBtn.innerHTML;
        }
      }
    }

    // Now we are ready to copy the constructed button back to the DOM.
    element.outerHTML = el.outerHTML;
    // We return the constructed button.
    return el;
  };
})(window['ara']);
