<?php

$p = '/themes/ara/lib/polymer/bower_components/';

ara_attach_html_head_link($p . 'paper-button/paper-button.html');

if($settings['icon']) {
  if ($settings['icon']['icon-set'] === 'icons') {
    ara_attach_html_head_link($p . 'iron-icons/iron-icons.html');
  }
  ara_attach_html_head_link($p . 'iron-icons/' . $settings['icon']['icon-set'] . '-icons.html');
}

ara_attach_html_head_link($p . 'polymer/polymer.html');
