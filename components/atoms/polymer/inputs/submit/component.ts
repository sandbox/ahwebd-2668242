(function (domready, ara, drupalSettings) {
  "use strict";

  // Component name.
  var cn = 'input--submit--polymer';
  // Component class name.
  var cl = cn + '-ara';

  // Get component settings
  var s = JSON.parse(drupalSettings.ara[cn]) || {};

  domready(function () {
    var elements = document.getElementsByClassName(cl);
    for (var i = 0; i < elements.length; i++) {
      ara.make.polymerButton(elements[i], s);
    }
  });
})(window['domready'], window['ara'], window['drupalSettings']);
