(function (ara) {
  "use strict";

  /**
   * Function to make a lumx button from another element.
   *
   * Note: If the provided element is <input>, this function might change it to <button>.
   *
   * @param {HTMLElement} element
   * @param {object} settings
   *
   * @return {HTMLElement}
   *   The styled lumx button.
   */
  ara.make.lumxButton = function (element, settings) {

    // Check if provided element exists.
    if (!element) { return null; }

    var retObj = ara.preprocessElement(element, 'lumx', settings);
    if (retObj) {
      var el = retObj.originalElement;
      settings = retObj.settings;
    } else {
      return null;
    }

    // Prepare classes to add to the button.
    var classes = " btn" +
    (ara.isSet(() => settings.type) ? " btn--" + settings.type : " btn--flat") +
    (ara.isSet(() => settings.size) ? " btn--" + settings.size : "") +
    (ara.isSet(() => settings.color) ? " btn--" + settings.color : "") +
    (ara.isSet(() => settings.bgc) ? " bgc-" + settings.bgc : "") +
    (ara.isSet(() => settings.tc) ? " tc-" + settings.tc : "");
    // Set classes
    el.className += classes;

    // If button should be disabled
    if (settings.disabled) {
      el.setAttribute("disabled", "");
    }

    // If button with mdi
    if ( ara.isSet(() => settings.mdi) ) {
      // For mdi we need to add a child element defining the icon,
      // <input> elements do not accept children, so we change to <button>
      // if provided element is <input>.
      inputToButton();

      // "fab" and "icon" types don't look good if there is text next to them,
      // so we remove it.
      if (settings.type === 'fab' || settings.type === 'icon') {
        el.innerHTML = '';
      }
      // For other types if there is text in the button we need to make spacing
      // between it and the icon.
      else if (el.innerHTML !== '') {
        el.innerHTML += ' ';
      }
      // We are now ready to insert the icon.
      el.innerHTML += '<i class="mdi mdi-' + settings.mdi + '"></i>';
    }

    // Ripple effect
    if (settings.ripple) {
      // Make sure ripple angular module will be loaded (if its library is added)
      ara.ngModsAddIfDefined('lumx.ripple');

      // <input> elements do not accept children but the ripple effect needs
      // to create children so we change to <button> if provided element is <input>.
      inputToButton();

      // Adding ripple effect
      el.setAttribute("lx-ripple",
        (ara.isSet(() => settings.rippleColor) ? settings.rippleColor : "")
      );
    }

    // Now we are ready to copy the constructed button back to the DOM.
    element.outerHTML = el.outerHTML;
    // We return the constructed button.
    return el;

    // Helper function to change button tagName from <input> to <button>
    function inputToButton() {
      if (el.tagName.toLowerCase() === "input") {
        // Put el inside a parent wrapper to allow assigning outerHTML.
        var elWrapper = document.createElement("div");
        elWrapper.innerHTML = el.outerHTML;
        el = <HTMLElement>elWrapper.firstChild;

        el.outerHTML = el.outerHTML.
          replace(/^<input/gi,"<button").
          replace(/<\/input>$/gi,"</button>");
        // After changing the outerHTML the el variable will still hold
        // reference to the original el, so re-assigning el again.
        el = <HTMLElement>elWrapper.firstChild;
        // If <input> has a "value" attribute, put its value in the innerHTML.
        if (el.hasAttribute("value")) {
          el.innerHTML += el.getAttribute("value");
        }
      }
    }
  };

})(window['ara']);
