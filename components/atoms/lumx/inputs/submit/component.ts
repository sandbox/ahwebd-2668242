(function (domready, ara, drupalSettings) {
  "use strict";

  // Component name.
  var cn = 'input--submit--lumx';
  // Component class name.
  var cl = cn + '-ara';
  // Component selector.
  var sel = '.' + cl;

  // Get component settings
  var s = JSON.parse(drupalSettings.ara[cn]) || {};

  domready(function () {
    var elements = document.getElementsByClassName(cl);
    for (var i = 0; i < elements.length; i++) {
      ara.make.lumxButton(elements[i], s);
    }
  });

})(window['domready'], window['ara'], window['drupalSettings']);
