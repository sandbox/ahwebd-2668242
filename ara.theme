<?php

/**
 * @file
 * Functions to support theming.
 */

use Symfony\Component\Yaml\Yaml;

/**
 * Implements hook_preprocess_HOOK().
 *
 * Adds body classes if certain regions have content.
 */
function ara_preprocess_html(&$variables) {
  // Adding layout classes to body tag depending on presence of sidebars.
  // Get sidebar regions if they are not empty.
  if (!empty($variables['page']['sidebar_first'])) {
    $s1 = $variables['page']['sidebar_first'];
  }
  if (!empty($variables['page']['sidebar_second'])) {
    $s2 = $variables['page']['sidebar_second'];
  }

  // When both sidebars present assign class 'two-sidebars' to the body.
  if (!empty($s1) && !empty($s2)) {
    $variables['attributes']['class'][] = 'two-sidebars';
  }
  // When only sidebar_first is present assign corresponding classes.
  elseif (empty($s1) && !empty($s2)) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-second';
  }
  // When only sidebar_second is present assign corresponding classes.
  elseif (!empty($s1) && empty($s2)) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-first';
  }
  // When no sidebars are present assign classe 'no-sidebars'.
  else {
    $variables['attributes']['class'][] = 'no-sidebars';
  }
}

/**
 * Implements hook_theme_suggestions_alter().
 *
 * Adds a new theme suggestion to allow ara enabled components override
 * corresponding html and load components libraries.
 */
function ara_theme_suggestions_alter(array &$suggestions, array $variables, $base_hook) {

  $components = ara_get_enabled_components();
  // Base hook components.
  $components = isset($components[$base_hook]) ? $components[$base_hook] : NULL;
  if (!empty($components)) {
    foreach ($components as $component => $properties) {
      // Suggestion to override.
      $suggestion = ara_component_to_suggestion($base_hook, $component);
      // Ara suggestion to add.
      $ara_suggestion = $suggestion . '__' . $properties['framework'] . $properties['suffix'] . '_ara';

      // If ara suggestion is not already added.
      if (!in_array($ara_suggestion, $suggestions)) {
        // If component suggestion is to override base hook suggestion.
        if ($component === '_SELF_') {
          // To ensure base hook ara suggestion is added right at the beginning
          // of the suggestions array we won't add it now, instead we store it
          // in another variable and we add it after processing all components
          // under the same base hook.
          $base_hook_ara_suggestion = $ara_suggestion;
        }
        else {
          // Search for the suggestion to override.
          $index = array_search($suggestion, $suggestions);
          if ($index !== FALSE) {
            // Insert ara suggestion exactly after the suggestion to override.
            $suggestions = array_merge(
              array_slice($suggestions, 0, $index + 1),
              [$ara_suggestion],
              array_slice($suggestions, $index + 1)
            );
          }
          // Sometimes base_hook and theme_hook_original are different
          // (ex: input and input__submit) so considering this case.
          elseif ($suggestion === $variables['theme_hook_original']) {
            // Add ara suggestion to the beginning of the suggestions array.
            array_unshift($suggestions, $ara_suggestion);
          }
        }
      }
    }
    if (isset($base_hook_ara_suggestion)) {
      // Add ara suggestion to the beginning of the suggestions array.
      array_unshift($suggestions, $base_hook_ara_suggestion);
    }
  }
}

/**
 * Implements hook_preprocess().
 *
 * Gets component js settings from a subtheme yaml file and sends them to
 * front-end via drupalSettings.
 * This function also loads additional library dependencies when declared in
 * extra-lib-dependencies.yml files.
 * It also loads component.inc files if present to allow additional php logic.
 */
function ara_preprocess(&$variables, $base_hook, $info) {
  // Check if ara component.
  if (substr($info['template'], -4) === '-ara') {

    $subthemes = ara_get_subthemes();
    $component_path = substr($info['path'], strlen($info['theme path']));

    // Array to hold component settings.
    $settings = [];
    foreach ($subthemes as $subtheme) {
      // Get component js settings from .yml file inside subtheme.
      $yml = DRUPAL_ROOT . '/themes/' . $subtheme . '/settings' . $component_path . '/settings.js.yml';
      if (file_exists($yml)) {
        // Merge settings.
        $settings = array_replace_recursive($settings,
          Yaml::parse(file_get_contents($yml))
        );
      }
    }
    // General settings.
    $gs = ara_get_general_settings();
    array_walk_recursive($settings, function (&$setting, $key, $gs) {
      // If a setting points to a general setting (its value prefixed by "gs.").
      if (substr($setting, 0, 3) === 'gs.') {
        // Assign the general setting value to the setting.
        if (isset($gs[substr($setting, 3)])) {
          $setting = $gs[substr($setting, 3)];
        }
      }
    }, $gs);

    // Element to render.
    $element = array();

    // Send settings to front-end.
    $element['#attached']['drupalSettings']['ara'][substr($info['template'], 0, -4)]
      = json_encode($settings);

    // Get additional libraries to load.
    $element['#attached']['library'] = [];
    foreach ($subthemes as $subtheme) {
      $yml = DRUPAL_ROOT . '/themes/' . $subtheme . '/settings' . $component_path .
      '/extra-lib-dependencies.yml';
      if (file_exists($yml)) {
        $libraries = Yaml::parse(file_get_contents($yml));
        if (isset($libraries)) {
          $element['#attached']['library'] += $libraries;
        }
      }
    }

    // Attach component settings library.
    $element['#attached']['library'] += ['ara/' . substr($info['template'], 0, -4) . '-settings'];

    // Include .inc files.
    $inc = DRUPAL_ROOT . '/' . $info['path'] . '/component.inc';
    if (file_exists($inc)) {
      require_once $inc;
    }

    foreach ($subthemes as $subtheme) {
      $inc = DRUPAL_ROOT . '/themes/' . $subtheme . '/includes' . $component_path . '/component.inc';
      if (file_exists($inc)) {
        require_once $inc;
      }
    }

    \Drupal::service('renderer')->render($element);
  }
}

/**
 * Implements hook_library_info_build().
 *
 * To facilitate contrib components distribution and installation we need to
 * have all component code enclosed in one directory. This icludes libraries
 * definitions that will be extracted from "library.yml" files residing in
 * component directories inside subthemes using this function.
 *
 * This function also builds a library for component css settings defined in
 * 'settings.css' files residing in subthemes.
 */
function ara_library_info_build() {
  $libraries = [];
  $components = ara_get_enabled_components();
  $subthemes = ara_get_subthemes();
  if (!empty($components)) {
    foreach ($components as $base_hook => $base_hook_components) {
      foreach ($base_hook_components as $component => $properties) {

        $lib_name = ara_component_to_suggestion($base_hook, $component);
        $lib_name .= '--' . $properties['framework'] . $properties['suffix'];
        $lib_name = str_replace('_', '-', $lib_name);

        // If component has 'library.yml' we use it to define its library.
        $library = NULL;
        $yml = DRUPAL_ROOT . '/' . $properties['path'] . '/library.yml';
        if (file_exists($yml)) {
          $yml_contents = file_get_contents($yml);
          // Replace _COMPONENT_PATH_ in library.yml with component path.
          $yml_contents = str_replace('_COMPONENT_PATH_', '../../' . $properties['path'], $yml_contents);
          $library = Yaml::parse($yml_contents);
        }
        if (isset($library)) {
          $libraries[$lib_name] = $library;
        }

        // If component has 'settings.css' files in subthemes we define
        // a library for them.
        $css = [];
        foreach ($subthemes as $subtheme) {
          $path = substr($properties['path'], 7);
          $path = substr($path, strpos($path, '/') + 1);
          $css_file = '/themes/' . $subtheme . '/settings/' . $path .
          '/settings.css';
          if (file_exists(DRUPAL_ROOT . $css_file)) {
            $css['../..' . $css_file] = [];
          }
        }
        if (!empty($css)) {
          $libraries[$lib_name . '-settings']['css']['base'] = $css;
        }
      }
    }
  }
  return $libraries;
}

/**
 * Implements ara_library_info_alter().
 *
 * Adds component rtl files to the component library.
 */
function ara_library_info_alter(&$libraries, $extension) {
  if ($extension === 'ara') {
    $gs = ara_get_general_settings();
    if (isset($gs['rtl']) && $gs['rtl']) {
      foreach ($libraries as $key => $library) {
        if (isset($library['css'])) {
          foreach ($library['css'] as $category => $files) {
            $new_files = [];
            foreach ($files as $file => $value) {
              $new_files[$file] = $value;
              $file_rtl = substr($file, 0, -4) . '-rtl.css';
              if (file_exists(DRUPAL_ROOT . '/themes/ara/' . $file_rtl)) {
                // @todo:
                // if in subtheme
                $new_files[$file_rtl] = $value;
              }
            }
            $libraries[$key]['css'][$category] = $new_files;
          }
        }
      }
    }
  }
}

// Helper functions.
/**
 * Builds component suggestion string.
 *
 * @param string $base_hook
 *   The base hook of the component.
 * @param string $component
 *   The component, '_SELF_' for base hooks.
 *
 * @return string
 *   The component suggestion.
 */
function ara_component_to_suggestion($base_hook, $component) {
  if ($component === '_SELF_') {
    $suggestion = $base_hook;
  }
  else {
    $suggestion = $base_hook . '__' . str_replace('-', '_', $component);
  }
  return $suggestion;
}

/**
 * Gets enabled components.
 *
 * This function parses "enabled-components.yml" files from subthemes, then
 * removes components without template (twig) files.
 *
 * @return array
 *    An array containing all enabled components.
 */
function ara_get_enabled_components() {
  static $ara_enabled_components;
  if (!isset($ara_enabled_components)) {
    $ara_enabled_components = [];
    $subthemes = ara_get_subthemes();

    // Parse "enabled-components.yml" files from subthemes.
    foreach ($subthemes as $subtheme) {
      $yml = DRUPAL_ROOT . '/themes/' . $subtheme .
      '/settings/components/enabled-components.yml';
      if (file_exists($yml)) {
        $ara_enabled_components = array_replace_recursive(
          $ara_enabled_components, Yaml::parse(file_get_contents($yml))
        );
      }
    }

    $gs = ara_get_general_settings();
    $hooks = theme_get_registry();

    $templates = [];
    foreach ($hooks as $hook) {
      $templates[] = $hook['template'];
    }

    foreach ($ara_enabled_components as $base_hook => $components) {
      foreach ($components as $component => $properties) {

        $framework = $properties['framework'];
        if (isset($properties['suffix'])) {
          $suffix = '__' . $properties['suffix'];
        }
        else {
          $suffix = '';
        }

        // If component framework points to the general settings framework.
        if (($framework === 'gs.framework') && isset($gs['framework'])) {
          // Assign the general settings framework to component framework.
          $framework = $gs['framework'];
        }

        $suggestion = ara_component_to_suggestion($base_hook, $component);
        $ara_suggestion = $suggestion . '__' . $framework . $suffix . '_ara';

        // If component does not have twig file.
        if (!in_array(str_replace('_', '-', $ara_suggestion), $templates)) {
          // Remove from $ara_enabled_components.
          unset($ara_enabled_components[$base_hook][$component]);
        }
        else {
          $ara_enabled_components[$base_hook][$component] = [
            "framework" => $framework,
            "suffix" => $suffix,
            "path" => $hooks[$ara_suggestion]['path'],
          ];
        }
      }
    }
    // Clean array from unset elements.
    $ara_enabled_components = array_filter($ara_enabled_components);
  }
  return $ara_enabled_components;
}

/**
 * Gets js general settings.
 *
 * @return array
 *    An array containing the general settings.
 */
function ara_get_general_settings() {
  static $gs;
  // If this function was already called, $gs will already have a value, so no
  // need to get it again.
  if (!isset($gs)) {
    $gs = [];
    $subthemes = ara_get_subthemes();
    foreach ($subthemes as $subtheme) {
      $yml = DRUPAL_ROOT . '/themes/' . $subtheme . '/settings/settings.js.yml';
      if (file_exists($yml)) {
        $gs = array_replace_recursive(
          $gs, Yaml::parse(file_get_contents($yml))
        );
      }
    }
  }
  return $gs;
}

/**
 * Gets the names of the active ara subthemes.
 *
 * @return array
 *    An array containing the names of the active ara subthemes.
 */
function ara_get_subthemes() {
  static $subthemes;
  if (!isset($subthemes)) {
    $active_theme = \Drupal::theme()->getActiveTheme()->getName();
    $base_themes = \Drupal::theme()->getActiveTheme()->getBaseThemes();

    $base_themes_names = array();
    foreach ($base_themes as $key => $value) {
      $base_themes_names[] = $key;
    }
    // Remove "stable" theme name.
    array_pop($base_themes_names);
    // Remove "ara" theme name.
    array_pop($base_themes_names);
    // Add active theme.
    array_unshift($base_themes_names, $active_theme);
    // Reverse themes order.
    $subthemes = array_reverse($base_themes_names);
  }
  return $subthemes;
}

/**
 * Attachs an html head link.
 *
 * @param string $href
 *   A string containing the link.
 */
function ara_attach_html_head_link($href) {
  // Prepare html head link tag.
  $link = array(array(
    'rel' => 'import',
    'href' => $href,
  ),
  );
  // Attach link tag.
  $element = array();
  $element['#attached']['html_head_link'][] = $link;
  \Drupal::service('renderer')->render($element);
}
