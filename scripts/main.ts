/**
 * @file
 * Main ts file holding Ara class and different helper functions.
 */


(function (window, drupalSettings, domready) {
  "use strict";

  class Ara {
    Drupal;
    make;
    preprocessElement(element, framework, settings) {}
    elements = [];
    idCounter = 0;
    ngMods = [];
    loadedLastFunctions = [];
    qs(selector, context) {}
    isSet(fn) {}
    replaceTagName(element, newTagName) {}
    t(str) {}
    objectMerge(obj1, obj2) {}
    ngModsAdd(mod) {}
    ngModsAddIfDefined(mod) {}
  }

  var ara = new Ara();

  // Object that will hold theming functions.
  ara.make = {};

  /**
   * Function that defines how an element is going to be styled, depending
   * whether it is the first time or it already been styled with ara.
   *
   * @param {HTMLElement} element
   * @param {string} framework
   * @param {object} settings
   *
   * @return {object}
   *   Object containing original element and settings to use for styling.
   */
  ara.preprocessElement = function (element, framework, settings) {
    var id = null;
    // Ensure "settings" is defined.
    settings = settings || {};
    // Ensure "weight" is defined.
    settings.weight = settings.weight || 0;

    // If element is not styled yet with ara.
    if (!element.hasAttribute('ara-id')) {
      ara.idCounter += 1;
      id = ara.idCounter;
      element.setAttribute('ara-id', id);
      ara.elements[id] = {
        original: element.cloneNode(true),
        framework: framework,
        settings: settings
      };
    }
    // If element was styled with ara.
    else {
      id = element.getAttribute('ara-id');

      // If new framework is the same framework used before to style the element.
      if (framework === ara.elements[id].framework) {
        // Merge settings (higher weight settings take precedence)
        if (settings.weight > ara.elements[id].settings.weight) {
          settings = ara.objectMerge(ara.elements[id].settings, settings);
        } else {
          settings = ara.objectMerge(settings, ara.elements[id].settings);
        }
      }
      // If different framework and lower weight, we won't style again.
      else if (settings.weight <= ara.elements[id].settings.weight) {
        return null;
      }

      // Update saved element framework and settings.
      ara.elements[id].framework = framework;
      ara.elements[id].settings = settings;
    }
    return {
      originalElement: ara.elements[id].original.cloneNode(true),
      settings: settings
    };
  }

  /**
   * querySelector function.
   *
   * @param {string} selector
   * @param {HTMLElement} context
   *
   * @return {HTMLElement}
   */
  ara.qs = function (selector, context) {
    context = context || document;
    return <HTMLElement>context.querySelector(selector);
  }

  /**
   * Function to check if a variable is set.
   */
  ara.isSet = function (fn) {
      var value;
      try {
          value = fn();
      } catch (e) {
          value = undefined;
      } finally {
          return value !== undefined;
      }
  }

  /**
   * Function to replace an element tag name.
   */
  ara.replaceTagName = function (element, newTagName) {
    if (!element) { return null; }

    // Create a replacement tag
    var replacement = document.createElement(newTagName);

    // Grab all of the element's attributes, and pass them to the replacement
    for (var i = 0, l = element.attributes.length; i < l; ++i) {
    	var nodeName  = element.attributes.item(i).nodeName;
    	var nodeValue = element.attributes.item(i).nodeValue;

    	replacement.setAttribute(nodeName, nodeValue);
    }

    // Copy contents
    replacement.innerHTML = element.innerHTML;

    return replacement;
  }

  /**
   * Translation function.
   */
  if (ara.isSet( () => window.Drupal )) {
    ara.t = window.Drupal.t;
  } else {
    ara.t = function (str) { return str; };
  }

  /**
   * Function to merge two objects recursively.
   *
   * @param {object} obj1
   * @param {object} obj2
   *
   * @return {object}
   */
  ara.objectMerge = function (obj1, obj2) {
    if (typeof obj1 !== 'object') { return null; }

    if (typeof obj2 === 'object') {
      for (var key in obj2) {
        if (obj1[key] && typeof obj1[key] === 'object') {
          obj1[key] = ara.objectMerge(obj1[key], obj2[key]);
        } else {
          obj1[key] = obj2[key];
        }
      }
    }
    return obj1;
  }

  /**
   * Function to add an angular module to the list of modules to be bootstrapped.
   *
   * @param {string} mod
   */
  ara.ngModsAdd = function (mod) {
    if (ara.ngMods.indexOf(mod) < 0) { ara.ngMods.push(mod); }
  }

  /**
   * Function to add an angular module to the list of modules to be bootstrapped
   * if this module is defined, use this if not sure the module is defined
   * (optional modules, ex: ripple)
   *
   * @param {string} mod
   */
  ara.ngModsAddIfDefined = function (mod) {
    try {
      window['angular']['module'](mod);
      if (ara.ngMods.indexOf(mod) < 0) { ara.ngMods.push(mod); }
    } catch(e) {}
  }

  domready(function () {
    // Append "loaded-last.js" to the end of body.
    var loadedLast = document.createElement("script");
    loadedLast.setAttribute("src", "/themes/ara/scripts/loaded-last.js");
    document.getElementsByTagName("body")[0].appendChild(loadedLast);
  });

  window.ara = ara;
})(window, window['drupalSettings'], window['domready']);
