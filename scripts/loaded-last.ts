/**
 * @file
 * This file will be loaded last.
 */

(function (ara, angular, domready) {
  "use strict";

  domready(function () {
    if ( (angular) && (ara.ngMods !== []) ) {
      // Bootstrap registered angular modules.
      angular.bootstrap(document.documentElement, ara.ngMods);
    }
    // Run registered functions to be run at the end of the page load.
    ara.loadedLastFunctions.forEach(function(fn) { fn(); });
  });
})(window['ara'], window['angular'], window['domready']);
